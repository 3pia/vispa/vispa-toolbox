#!/usr/bin/env python

# run this via:
#  submit -f -q 2 ./test_tf.py
#  submit -f -q 2 -M 1000 ./test_tf.py

from multiworker import tf_mirror, NotACondorJob
import numpy as np
import tensorflow as tf

try:
    idx, num, strategy = tf_mirror()
except NotACondorJob:
    idx, num = 0, 1

    # use a local strategy instread, so we can switch without removing the `with ...:` line
    strategy = tf.distribute.MirroredStrategy()

with strategy.scope():
    model = tf.keras.Sequential([tf.keras.layers.Dense(1, input_shape=(1,))])

model.compile(loss="mse", optimizer="sgd")

inputs, targets = np.ones((100, 1)), np.ones((100, 1))
model.fit(inputs, targets, epochs=2, batch_size=10 // num)
