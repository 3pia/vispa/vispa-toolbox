from errno import ENOTEMPTY
from operator import attrgetter
from os import environ
from pathlib import Path
from random import random
from socket import create_server, AF_INET
from time import sleep, time
from typing import Any, Callable, Dict, Optional, Tuple, List, Set, Union
from weakref import finalize
import re
import logging

import htcondor
import classad

logger = logging.getLogger(__name__)

var_run = Path.home().joinpath(".local", "var", "run")


def wait(msg: str = "", extra: float = 0):
    time = 0.5 + 2 * random() + extra
    logger.debug(f"waiting {time:.1f}s {msg}")
    sleep(time)


class FSIPC:
    def __init__(self, base: Path, name: str):
        self.base = Path(base)
        self.base.mkdir(parents=True, exist_ok=True)
        self._own = self.base / name
        self._own.mkdir()
        self.name = name
        finalize(self, self._cleanup, self._own)

    @staticmethod
    def _cleanup(own: Path):
        wait(f"delayed ipc cleanup: {own}", 5)
        for item in list(own.iterdir()):
            item.unlink()
        own.rmdir()
        try:
            own.parent.rmdir()
        except OSError as e:
            if e.errno != ENOTEMPTY:
                pass

    @property
    def members(self) -> Set[str]:
        return set(map(attrgetter("name"), self.base.iterdir()))

    @property
    def peers(self) -> Set[str]:
        return self.members - {self.name}

    def __getitem__(self, key: str) -> Dict[str, str]:
        ret = {}
        for d in self.base.iterdir():
            try:
                ret[d.name] = d.joinpath(key).read_text()
            except FileNotFoundError:
                pass
        return ret

    def __setitem__(self, key: str, value: str):
        self._own.joinpath(key).write_text(value)

    def __contains__(self, peer_key: Tuple[str, str]) -> bool:
        peer, key = peer_key
        return self.base.joinpath(peer, key).exists()

    def waitfor(
        self, expect: Set[str], key: Optional[str] = None
    ) -> Optional[Dict[str, str]]:
        ret = None
        last = set()
        while expect - (
            now := (self.members if key is None else set((ret := self[key]).keys()))
        ):
            if now < last:
                raise RuntimeError("previous member vanished")
            last = now

            wait(f"waiting for ipc peers ({key}): {now} < {expect}")

        logger.debug(f"got all ({key=})")

        return ret


class NotACondorJob(RuntimeError):
    ...


def job_attrs() -> classad.ClassAd:
    if job_ad := environ.get("_CONDOR_JOB_AD"):
        with open(job_ad, "rt") as f:
            return classad.parseOne(f)
    else:
        raise NotACondorJob()


def job_sched() -> Tuple[Tuple[int, int], htcondor.Schedd]:
    ja = job_attrs()

    cid = ja["ClusterId"]
    pid = ja["ProcId"]

    sched, jid, qdate = ja["GlobalJobId"].split("#")
    assert jid == f"{cid}.{pid}"
    assert qdate == str(ja["QDate"])

    return (cid, pid), htcondor.Schedd(
        htcondor.Collector().locate(htcondor.DaemonTypes.Schedd, sched)
    )


def jid_peers() -> Tuple[int, int, Dict[int, str]]:
    (cid, pid), sched = job_sched()
    logger.debug(f"got jobid: {cid}.{pid}")

    # get number of peers & wait for all to run
    while True:
        t = time()
        jobs = sched.query(f"ClusterId == {cid}", ["ProcId", "JobStatus", "RemoteHost"])
        t = time() - t
        stati = set(job["JobStatus"] for job in jobs)
        logger.debug(f"status {stati=}")
        if stati == {2}:
            break
        if stati & {3, 4}:
            raise RuntimeError("some peers already removed/completed")

        wait(f"jobs to start, {stati=}", min(t**2, 15 * 60))

    # now we have the peers
    peers = {job["ProcId"]: job.get("RemoteHost", "").split("@", 1)[-1] for job in jobs}
    logger.debug(f"got {peers=}")

    return (cid, pid), peers


def peer_setup() -> Tuple[int, int, List[str]]:
    # allocate socket
    with create_server(("", 0), reuse_port=True) as sock:
        _, port = sock.getsockname()
        assert _ == "0.0.0.0"

        # get job id
        (cid, pid), peers = jid_peers()

        ipc = FSIPC(var_run.joinpath("FSIPC", f"htcondor_{cid}"), str(pid))
        ipc["port"] = str(port)
        ports = ipc.waitfor(set(map(str, peers.keys())), "port")
        logger.debug(f"got {ports=}")

    return pid, port, [f"{peers[i]}:{ports[str(i)]}" for i in sorted(peers.keys())]


def ext_if(
    af=AF_INET, *, ignore: Callable[[str], bool] = re.compile("lo|docker").match
) -> Dict[str, str]:
    """
    returns a dictionary of external interfaces to adresses, for the given address family
     (*af*, default IPv4), respecting *ignore*s
    """
    from psutil import net_if_addrs

    return {
        name: addr.address
        for name, addrs in net_if_addrs().items()
        if not ignore(name)
        for addr in addrs
        if addr.family == af
    }


def tf_mirror() -> Tuple[int, int, "tf.distribute.MultiWorkerMirroredStrategy"]:
    """
    Setus up a MultiWorkerMirroredStrategy for tensorflow.
    Returns: own_index, number_of_peers, ready_to_use_strategy
    """
    import tensorflow as tf

    idx, _, peers = peer_setup()
    nGPU = len(tf.config.list_physical_devices("GPU"))

    print(f"tf_mirror[{idx}/{len(peers)}]({nGPU} GPU) {peers[idx]}", flush=True)

    spec = tf.train.ClusterSpec(dict(worker=peers))
    rslv = tf.distribute.cluster_resolver.SimpleClusterResolver(
        spec,
        task_type="worker",
        task_id=idx,
        num_accelerators=dict(GPU=nGPU),
    )
    strat = tf.distribute.MultiWorkerMirroredStrategy(cluster_resolver=rslv)

    return idx, len(peers), strat


def torch_ipg(gpu=None) -> Tuple[int, int]:
    """
    initalizes torch.distributed, with appropriate backend for *gpu* presence, autodetected if *None*
    Returns: own_index, number_of_peers
    """
    import torch

    (cid, pid), peers = jid_peers()

    # fix for get `nslookup $(hostname)` returning a loopback address (127.0.1.1)
    ifs = list(ext_if().keys())
    environ["GLOO_SOCKET_IFNAME"] = ",".join(ifs)
    # environ["NCCL_SOCKET_IFNAME"] = ",".join(ifs) # not necessary for NCCL

    print(f"torch_ipg[{pid}/{len(peers)}] {peers[pid]}", flush=True)

    assert torch.distributed.is_available()
    assert not torch.distributed.is_initialized()

    if gpu is None:
        gpu = torch.cuda.is_available()

    if gpu:
        assert torch.cuda.is_available()
        assert torch.distributed.is_nccl_available()
        backend = "nccl"
    else:
        assert torch.distributed.is_gloo_available()
        backend = "gloo"

    store = var_run.joinpath("torch", f"distributed.{cid}.store")
    store.parent.mkdir(parents=True, exist_ok=True)

    torch.distributed.init_process_group(
        backend, init_method=store.as_uri(), world_size=len(peers), rank=pid
    )

    assert torch.distributed.is_initialized()

    return pid, len(peers)
