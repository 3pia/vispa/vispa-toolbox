#!/usr/bin/env python

# run this via:
#  submit -f -q 2 ./test_torch.py
#  submit -f -q 2 -M 1000 ./test_torch.py

import torch
import torch.nn as nn
import torch.optim as optim
from multiworker import NotACondorJob, torch_ipg


try:
    rank, world_size = torch_ipg()
except NotACondorJob:
    rank, world_size = 0, 1


class ToyModel(nn.Module):
    def __init__(self):
        super(ToyModel, self).__init__()
        self.net1 = nn.Linear(10, 10)
        self.relu = nn.ReLU()
        self.net2 = nn.Linear(10, 5)

    def forward(self, x):
        return self.net2(self.relu(self.net1(x)))


device = torch.cuda.current_device() if torch.cuda.is_available() else None

model = ToyModel()
model = model.to(device)

if world_size > 1:
    # device_ids not needed, since it's only one gpu anyway
    ddp_model = nn.parallel.DistributedDataParallel(model)

loss_fn = nn.MSELoss()
optimizer = optim.SGD(ddp_model.parameters(), lr=0.001)

optimizer.zero_grad()
outputs = ddp_model(torch.randn(20, 10))
labels = torch.randn(20, 5).to(device)
loss_fn(outputs, labels).backward()
optimizer.step()

print("done!?")
